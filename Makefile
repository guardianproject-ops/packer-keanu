PROJECT_ROOT ?= ../
include $(PROJECT_ROOT)/Makefile.env
HELP_EXTRA = "Where APPS is one of: $(APPS)"
export KEANU_ANSIBLE_PATH ?= ../ansible-base
export PACKER ?= packer
export PACKER_JSON ?= packer.json
export PACKER_OUTPUT ?= ../data/packer
export AMI_KMS_KEY_ID ?=
export ANSIBLE_EXTRA_ARGS ?= -v
export AMI_KIND ?=

APPS = debian-base ma1sd synapse sygnal prometheus

APPS_GOLDEN = $(patsubst %, golden/%,$(APPS))
APPS_BAKED = $(patsubst %, baked/%,$(APPS))
APPS_AMI= $(patsubst %, ami/%,$(APPS))

$(call check_defined, NAMESPACE)
$(call check_defined, ENVIRONMENT)
$(call check_defined, PROJECT)
$(call check_defined, AWS_REGION)
$(call check_defined, AWS_PROFILE, used for aws authentication)
$(call check_defined, PACKER_OUTPUT, where to put the packer manifests)
$(call check_defined, KEANU_ANSIBLE_PATH, path to ansible playbooks)
$(call check_defined, PACKER, the packer binary)
$(call check_defined, PACKER_JSON, packer.json)
$(call check_defined, AMI_KMS_KEY_ID, kms key for boot encryption)

## prepare ansible dependencies
ansible-deps:
	cd $(KEANU_ANSIBLE_PATH) && $(MAKE) deps

.PHONY: $(APPS_AMI)
$(APPS_AMI):
	$(call assert-set,AMI_KIND)
	@echo building ami $(@F)
	@echo ENCRYPTION: $(AMI_KMS_KEY_ID)
	. $(@F)/versions && $(PACKER) build $(PACKER_ARGS) -timestamp-ui $(@F)/$(PACKER_JSON)

.SECONDEXPANSION:
.PHONY: $(APPS_GOLDEN)
$(APPS_GOLDEN): export AMI_KIND=gold
$(APPS_GOLDEN): export ANSIBLE_RUN_TAGS=install
$(APPS_GOLDEN): ami/$$(@F)

.PHONY: $(APPS_BAKED)
$(APPS_BAKED): export AMI_KIND=baked
$(APPS_BAKED): ami/$$(@F)

# these are dummy targets for the help text
## build golden ami (install software only)
golden/$(APPS):
	@echo built golden $(@F)

### build ami baked with project specific settings
baked/$(APPS):
	@echo built baked $(@F)
